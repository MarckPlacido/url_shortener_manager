// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/



var initFunctionDash = function(){

  $('#btn_gnl').on('click', function(event){
		event.preventDefault();
    var url_or = this.parentElement.parentElement.children[0].value

    if (url_or == '' || !isValidURL(url_or)) {
      clearCantainerMG()
      var html_error_b = ''
			html_error_b += '<h5 style="font-size: 45px; text-align: center; color: orange;">';
			html_error_b += '<br>Atención</h5>';
			html_error_b += '<p style="text-align: center;" >';
			html_error_b += '<b>';
			html_error_b += 'Favor de ingresar un enlace para acortarlo';
			html_error_b += '</b>';
			html_error_b += '</p>';
			$("#body_MG").append(html_error_b);

      var html_error_f = '<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>';
			$("#footer_MG").append(html_error_f);

      $('#modal_generic').modal('toggle');
      return false
    }

    console.log("clic2 ", this.parentElement.parentElement.children[0].value);
    this.value = ""
    $.ajax({
      type: "POST",
      data: {url_or: url_or},
      url: "/log_short_links/generate_link",
      dataType: 'JSON',
      success: function(data){
        console.log(">>>>>>>>>. ", data);
        var html_u = ""
        html_u += '<tr id="'+data.id+'">'
        html_u += '<td style="font-size: 13px;"><a href="'+data.link_original+'">'+data.link_original+'</a></td>'
        html_u += '<td><a href="'+data.link_short+'">'+data.link_short+'</a></td>'
        html_u += '<td>'+data.created_at+'</td>'
        html_u += '<td><button type="button" name="button" class="btn btn-sm btn-danger" onclick="deleteRow(this)" data-id-ls="'+data.id+'"> - </button></td>'
        html_u += '<td><button type="button" name="button" class="btn btn-sm btn-warning" onclick="updateLink(this)" data-id-ls="'+data.id+'"> ? </button></td>'
        html_u += '</tr>'

        $("#tbl_urlS").append(html_u)
      },
      error: function(data){
        console.log('Error:');
      }
    });

  })

}

var input_url;
function isValidURL(u){
  if(!input_url){
    input_url = document.createElement('input');
    input_url.setAttribute('type', 'url');
  }
  input_url.value = u;
  return input_url.validity.valid;
}


function deleteRow(e){
  var id_sl = $(e).attr("data-id-ls")
  $.ajax({
    type: "POST",
    data: {id: id_sl},
    url: "/log_short_links/delete_url",
    dataType: 'JSON',
    success: function(data){
      e.parentNode.parentNode.remove()
    },
    error: function(data){
      console.log('Error:');
    }
  });
}

function updateLink(e){
  var id_sl = $(e).attr("data-id-ls")
  clearCantainerMG()
  var html_b = ''
  html_b += '<table class="table table-condesed">'
  html_b += '<colgroup>'
  html_b += '<col style="width: 25%"/>'
  html_b += '<col style="width: 75%"/>'
  html_b += '</colgroup>'
  html_b += '<tr><td>Url acortado</td><td>'+e.parentNode.parentNode.children[1].children[0].textContent+'</td></tr>'
  html_b += '<tr><td>Url destino</td><td><input  pattern="http://.*" required="" style="font-size:13px;" type="url" class="form-control" name="" value="'+e.parentNode.parentNode.children[0].children[0].textContent+'"></td></tr>'
  html_b += '</table>'
  $("#body_MG").append(html_b);

  var html_f = '<button data-id-ls="'+id_sl+'" type="button" class="btn btn-primary" data-id-url="" onclick="updateLinkOrig(this)">OK</button>';
  $("#footer_MG").append(html_f);

  $('#modal_generic').modal('toggle');
}

function updateLinkOrig(e){
  var input_obj = e.parentNode.parentNode.children[1].getElementsByTagName('input')
  var id_sl = $(e).attr("data-id-ls")
  if (isValidURL(input_obj[0].value)) {
    $.ajax({
      type: "POST",
      data: {id: id_sl, url_new: input_obj[0].value},
      url: "/log_short_links/update_url",
      dataType: 'JSON',
      success: function(data){
        $("#"+id_sl).children()[0].children[0].textContent = input_obj[0].value
        $("#"+id_sl).children()[0].children[0].href = input_obj[0].value
        $('#modal_generic').modal('toggle');
      },
      error: function(data){
        console.log('Error:');
      }
    });
  }

}


function clearCantainerMG(){
  $("#header_MG").empty();
  $("#body_MG").empty();
  $("#footer_MG").empty();
}

$(document).ready(initFunctionDash);
$(document).on('page:load', initFunctionDash);
