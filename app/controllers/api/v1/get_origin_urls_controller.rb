module Api
  module V1
    class GetOriginUrlsController < ActionController::Base
      require 'socket'
        def find_url_origin
          ip = Socket.ip_address_list.detect{|intf| intf.ipv4_private?}
          @lsl = LogShortLink.find_by(link_short:params[:url])

          if !@lsl.blank?
            ##id log_short_links
            ##navegador
            #ip
            newLog = LogViewLink.new
            newLog.log_short_link_id = @lsl.id
            newLog.os_view = params[:web_nav]
            newLog.ip_view = ip.ip_address
            newLog.save
          end

          render json: @lsl
        end
    end
  end
end
