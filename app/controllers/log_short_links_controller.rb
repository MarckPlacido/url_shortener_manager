class LogShortLinksController < ApplicationController

  def generate_link
      print params.inspect

      created_code_status = false

      while !created_code_status  do
        #si se tuviera subdominios activos se realizaria una consulta para seleccionar una y agregarselo en lugar del localhost
        code_g = "http://localhost:3000/" + ([*('A'..'Z'),*('0'..'9'),*('a'..'z')]-%w(0 1 I O)).sample(8).join
        created_code_status = LogShortLink.find_by(:link_short => code_g).blank? ? true : false
      end

      sls_nA = {}
      @sls_n = LogShortLink.new
      @sls_n.link_original = params[:url_or]
      @sls_n.link_short = code_g
      @sls_n.status = true
      @sls_n.user_id = current_user.id

      if @sls_n.save
        print ">>>>>>>>>>>>>> " + @sls_n.id.to_s
        sls_nA[:id] = @sls_n.id
        sls_nA[:link_original] = @sls_n.link_original
        sls_nA[:created_at] = @sls_n.created_at.to_datetime.strftime("%d-%m-%Y")
        sls_nA[:link_short] = @sls_n.link_short
      end


      render :json => sls_nA
  end

  def delete_url
    @usl = LogShortLink.find(params[:id])
    @usl.destroy
    render :json => 200
  end

  def update_url
    @usl = LogShortLink.find(params[:id])
    @usl.update_attributes(:link_original => params[:url_new])
    render :json => 200
  end

end
