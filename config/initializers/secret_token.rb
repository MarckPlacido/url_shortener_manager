# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
UrlShortenerManager::Application.config.secret_key_base = '02316f9fb3a6c03d2c5ecb702e1d8e39c90aac8e75ec2c65c235558f7761001e8d1097491688f5aa7562913da5116c31baf26ab4027deb10ecc2aa28b24a0de1'
