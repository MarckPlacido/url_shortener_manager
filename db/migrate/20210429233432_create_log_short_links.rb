class CreateLogShortLinks < ActiveRecord::Migration
  def change
    create_table :log_short_links do |t|
      t.text :link_original
      t.text :link_short, unique: true
      t.boolean :status
      t.integer :user_id

      t.timestamps
    end
  end
end
