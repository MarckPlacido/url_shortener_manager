class CreateLogViewLinks < ActiveRecord::Migration
  def change
    create_table :log_view_links do |t|
      t.integer :log_short_link_id
      t.string :os_view
      t.string :ip_view

      t.timestamps
    end
  end
end
