def product(e)
    val_m = nil
    for i in e
        val_m = val_m.nil? ? i : val_m * i
    end
    val_m
end

def anagram?(a,b)
    anagram_s = (a.size === b.size)

    if anagram_s
        a.upcase.each_char { |aa|
            anagram_s = b.upcase.include?(aa)
            break if !b.upcase.include?(aa)
        }
    end
    return anagram_s
end

def compare(a,b)
    compare_s = (a.size === b.size) ? a.upcase.include?(b.upcase) : false
end

def sort_keys(hash_v)
    new_arr = []
    hash_v.each do |k,v|
        new_arr.push(k.to_s)
    end
    new_arr.sort { |a,b| a.length <=> b.length }

    new_arr
end
